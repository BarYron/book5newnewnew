@yield('content')
@extends('layouts.app')
@section('content')


<h1>Edit book</h1>

<form method = 'post' action = "{{action('Book5Controller@update', $book5->id)}}" >

@csrf
@method('PATCH')

<div class = "form-group">
    <label for = "title" > book toUpdate </label>
    <input type = "text" class = "form-control" name = "title" value="{{$book5->title}}">
</div>

<div class = "form-group">
    <label for = "title" > author toUpdate </label>
    <input type = "text" class = "form-control" name = "author" value="{{$book5->author}}">
</div>

<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "save">
</div>

</form>


<form method = 'post' action = "{{action('Book5Controller@destroy', $book5->id)}}" >

@csrf
@method('DELETE')

<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Delete">
</div>

</form>

@endsection


