<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'amit',
                    'email' => 'amit@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                     'role'=>'employee',
                ],

                [
                    'name' => 'bar',
                    'email' => 'bar@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                     'role'=>'employee',
                ],

                [
                    'name' => 'yaniv',
                    'email' => 'yaniv@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                     'role'=>'employee',
                ],
            ]);
    }
}
