<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Yarden Muallem',
                    'email' => 'yarden@gmail.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}
